<?php
session_start();
require './../vendor/autoload.php';
use App\Config\Router;
use App\Core\Database;

$uri = explode("/", $_SERVER['REQUEST_URI']);
$path = array_slice($uri, 3);

$router = new Router();
$router->resolve($path);

// $db = Database::connect();
// $idUser = 
// $chapo = "chapo";
// $auteur = "Hashim";
// $contenu = "this is contenu";
// $image = "random link"; 

// try{
// date_default_timezone_set('Europe/Paris');
// $dateModification = date('Y-m-d'); 
// // set the \PDO error mode to exception
// $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
// $sql = "UPDATE article SET chapo='$chapo', auteur='$auteur', dateModification='$dateModification', contenu='$contenu', image='$image' WHERE id='$id'";
// // Prepare statement
// $stmt = $db->prepare($sql);
// // execute the query
// $stmt->execute();
// } catch(\PDOException $e) {
// echo $sql . "<br>" . $e->getMessage();
// }


<?php

namespace App\Config;
use App\Controllers\HomeController;
use App\Controllers\BlogController;
use App\Controllers\AccountController;
use App\Controllers\SignUpController;
use App\Controllers\LogInController;
use App\Controllers\ErrorController;

class Router {
    public function resolve(array $path):void
    {

        $homeController = new HomeController();
        $blogController = new BlogController();
        $accountController = new AccountController();
        $signUpController = new SignUpController();
        $logInController  = new LogInController();
        $errorController = new ErrorController();
        switch ($path[0]){
            case "":
                $homeController->index();
                break;
            case "blog":
                if(count($path) == 1){
                    $blogController->index();
                }else if (count($path) == 2){
                    $blogController->article($path[1]);
                 } else{
                    $errorController->error404();
                 }
                break;
            case str_starts_with($path[0], 'signUp'):
                if (isset($_POST["submit"])){
                    $signUpController->addUser();
                } else {
                    $signUpController->index();
                }
                break;
            case str_starts_with($path[0], 'account'):
                if(isset($_SESSION["state"]) && $_SESSION["state"] === "connected"){
                    
                    if(count($path)==1 or $path[1] == ""){
                        $accountController->index();
                    } else if ($path[1] == "profile"){
                        if (isset($_POST["submitUpdate"])){
                            $accountController->updateUser();
                        }
                        else if (isset($path[2])){
                            $accountController->profilePassword($_SESSION["idUser"]);
                            
                    } else {
                        $accountController->profile($_SESSION["idUser"]);
                    }
                    } 
                     else if ($path[1] == "articles"){
                        if (isset($path[2])){
                            if ($path[2] == "add"){
                                $accountController->addArticle();
                            }
                            else if ($path[2] == "update"){
                                $accountController->updateArticle($path[3]);
                            }
                            else if ($path[2] == "delete"){
                                $accountController->deleteArticle($path[3]);
                            }
                            else if ($path[2] == "validate"){
                                $accountController->validateArticle($path[3]);
                            }
                            else if ($path[2] == "unvalidate"){
                                $accountController->unValidateArticle($path[3]);
                            }
                        }
                        else {
                            $accountController->articles($_SESSION["idUser"]);
                        }
                    }
                    else if ($path[1] == "comments"){
                       if (isset($path[2])){
                           if ($path[2] == "update"){
                               $accountController->updateComment(intval($path[3]));
                           }
                            else if ($path[2] == "delete"){
                               $accountController->deleteComment(intval($path[3]));
                           }
                           else if ($path[2] == "validate"){
                            $accountController->validateComment($path[3]);
                            }
                           else if ($path[2] == "unvalidate"){
                            $accountController->unValidateComment($path[3]);
                            }
                       } else { 
                          $accountController->comments(intval($_SESSION["idUser"]));
                       }
                   }
                   else if ($path[1] == "admin"){
                      if (isset($path[2])){
                          if ($path[2] == "articles"){
                              $accountController->adminArticles();
                          }
                          if ($path[2] == "comments"){
                              $accountController->adminComment();
                          }
                      } else { 
                         $accountController->comments($_SESSION["idUser"]);
                      }
                  } else {
                        $errorController->error404();
                    }

                } else if (isset($_POST["submit"])){
                    $logInController->logIn();
                } else {
                    $logInController->index();
                }
                break;
            case str_starts_with($path[0], 'disconnect'):
                session_destroy();
                header("Location: http://localhost:86/openblog/public/account");
                break;
            default:
                $errorController->error404();
        }
    }
}
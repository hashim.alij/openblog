<?php

use App\Core\Database;
class UserRepository{
    private $db;
    
    public function __construct()
    {
        $this->db = Database::connect(); 
    }

    public function findAll()
    {
        $stmt = "SELECT nom, prenom, role from user";
        $rows = $this->db->query($stmt);
        $users = $rows->fetchAll();
        return $users;
    }
}
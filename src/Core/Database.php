<?php

namespace App\Core;

class Database {
    protected static $connection = null;

    private function __construct()
    {
        echo "New connection instance is created !";
    }

    public static function connect(){
        $servername = "localhost";
        $username = "ahme0012";
        $password = "5VtX6sOdt5G1y8JK0J";

        try {

            if (!self::$connection){
                $conn = new \PDO("mysql:host=$servername;dbname=OpenBlog", $username, $password);
                // set the PDO error mode to exception
                $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                self::$connection = $conn;
            }
            return self::$connection;
        } catch(\PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }   
    }
}
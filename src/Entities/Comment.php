<?php

namespace App\Entities;

use DateTime;

class Comment{
    private int $idUser;
    private int $idArticle;
    private string $username;
    private DateTime $date;
    private string $text;

    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param int $idUser
     */
    public function setIdUser(int $idUser)
    {
        $this->idUser = $idUser;
    }

    public function getUsernname()
    {
        return $this->username;
    }

    /**
     * @param string $usernname
     */
    public function setUsernname(string $username)
    {
        $this->username = $username;
    }

    public function getIdArticle()
    {
        return $this->idArticle;
    }

    /**
     * @param int $idArticle
     */
    public function setIdArticle(int $idArticle)
    {
        $this->idArticle = $idArticle;
    }

    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate(DateTime $date)
    {
        $this->date = $date;
    }

    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }
}
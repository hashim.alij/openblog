<?php 
namespace App\Entities;

use DateTime;

class Article {
    private int $idUser;
    private string $lede;
    private string $author;
    private DateTime $date;
    private string $text;
    private string $image;

    public function getIdUser()
    {
        return $this->idUser;
    }

    public function getLede()
    {
        return $this->lede;
    }

    /**
     * @param string $lede
     */
    public function setLede(string $lede)
    {
        $this->lede = $lede;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor(string $author)
    {
        $this->author = $author;
    }

    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate(DateTime $date)
    {
        $this->date = $date;
    }

    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image)
    {
        $this->image = $image;
    }
}
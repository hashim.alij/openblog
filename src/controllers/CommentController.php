<?php 
namespace App\Controllers;
use App\Core\Database;
use PDO;
use PDOException;

class CommentController{
    private $db;
    
    public function __construct()
    {
        $this->db = Database::connect(); 
    }

    public function findAll()
    {
        $stmt = "SELECT * from comment";
        $rows = $this->db->query($stmt);
        $comments = $rows->fetchAll();
        return $comments;
    }

    public function myComments($id)
    {
        $stmt = "SELECT * from comment where id_user =$id";
        $rows = $this->db->query($stmt);
        $comments = $rows->fetchAll();
        return $comments;
    }

    public function articleComs($id)
    {
        $stmt = "SELECT * from comment where id_article=$id and status='valid'";
        $rows = $this->db->query($stmt);
        $comments = $rows->fetchAll();
        return $comments;
    }
    

    public function find($id)
    {
        $stmt = "SELECT * from comment where id = $id";
        $row = $this->db->query($stmt);
        $comment = $row->fetch(PDO::FETCH_OBJ);
        return $comment;
    }

    public function create($id, $text):bool{
        try {
            date_default_timezone_set('Europe/Paris');
            $date = date('Y-m-d'); 
            $idUser = $_SESSION["idUser"];
            $username = $_SESSION["username"];
            // set the PDO error mode to exception
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO comment (id_user, id_article, username, status, date, text)
            VALUES ('$idUser','$id', '$username', 'pending','$date', '$text')";
            $this->db->exec($sql);
            return true;
          } catch(PDOException $e) {
            return false;
          }
    }


    /**
     * @param int $id
     * @param string $text
     */
    public function update(int $id, string $text){
        try {
            // set the PDO error mode to exception
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE comment SET text='$text' WHERE id='$id'";
            // Prepare statement
            $stmt = $this->db->prepare($sql);
            // execute the query
            $stmt->execute();
          } catch(PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
          }
    }

    public function validate(int $id):bool{
      try {
          // set the PDO error mode to exception
          $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $sql = "UPDATE comment SET status='valid' WHERE id='$id'";
          // Prepare statement
          $stmt = $this->db->prepare($sql);
          // execute the query
          $stmt->execute();
          return true;
        } catch(PDOException $e) {
          echo $sql . "<br>" . $e->getMessage();
        }
  }

    public function unValidate(int $id):bool{
      try {
          // set the PDO error mode to exception
          $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $sql = "UPDATE comment SET status='pending' WHERE id='$id'";
          // Prepare statement
          $stmt = $this->db->prepare($sql);
          // execute the query
          $stmt->execute();
          return true;
        } catch(PDOException $e) {
          echo $sql . "<br>" . $e->getMessage();
        }
  }

    public function delete(int $id){
        try {
            // set the PDO error mode to exception
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "DELETE FROM comment WHERE id='$id'";
            // Prepare statement
            $stmt = $this->db->prepare($sql);
            // execute the query
            $stmt->execute();
            // execute the query
          } catch(PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
          }
    }

    public function count($idUser){
      $stmt = "SELECT COUNT(*) FROM comment where id_user=$idUser";
      $res = $this->db->query($stmt);
      $count = $res->fetchColumn();
      return $count;
    }
}
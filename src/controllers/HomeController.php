<?php

namespace App\Controllers;
use PHPMailer\PHPMailer\PHPMailer;
class HomeController extends AbstractController {


    public function index():void
    {
        $mailInfo = false;
        if (isset($_POST["sendMessage"])){
            // Example usage:
            $to = "hashim.alij@gmail.com";
            $subject = $_POST["lname"] . " " . $_POST["fname"];
            $message = $_POST["message"];
            $from = $_POST["email"];
            $mailInfo = $this->sendEmail($to, $subject, $message, $from, $_POST["lname"]);
        } else if (isset($_POST["downloadPDF"])) {
            $this->downloadPDF();
        }
            echo $this->twig->render('home/index.html.twig', ['name' => 'Hashim',  'mailInfo' => $mailInfo]);
        
    }

    public function sendEmail($to, $subject, $message, $from, $name):bool {
    $phpmailer = new PHPMailer();
    $phpmailer->isSMTP();
    $phpmailer->Host = 'sandbox.smtp.mailtrap.io';
    $phpmailer->SMTPAuth = true;
    $phpmailer->Port = 2525;
    $phpmailer->Username = '781c0c69214d88';
    $phpmailer->Password = '07d6b94756b8c0';
    $phpmailer->setFrom($from, $name);
    $phpmailer->addAddress('hashim.alij@gmail.com', 'AHMED ALI');
    $phpmailer->Subject = $subject;
    $phpmailer->Body = $message;

    // Attempt to send the email
    if($phpmailer->send()) {
        return true;
        } else {
            // echo 'Erreur de Mailer : ' . $phpmailer->ErrorInfo;
        return false;
        }
    }


    public function downloadPDF() {
        // Path to the PDF file in the assets directory
        $pdfFilePath = "./../assets/myCV.pdf";

        // Check if the file exists
        if (!file_exists($pdfFilePath)) {
            echo "Error: PDF file not found in assets directory.";
            return;
        }

        $savePath = "downloaded_file.pdf";

        // Save the PDF file
        if (file_put_contents($savePath , fopen($pdfFilePath, 'r'))) {
            return true;
        } else {
            return false;
        }
    }
}
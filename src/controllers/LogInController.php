<?php

namespace App\Controllers;
use App\Controllers\UserController;

class LogInController  extends AbstractController {


    public function index():void
    {
        echo $this->twig->render('connection/logIn.html.twig');
    }

    public function logIn():void
    {
        $email = strtolower($_POST["email"]);
        $password = $_POST["password"];
        $userControl = new UserController();
        $connect = $userControl->logIn($email, $password);
        if($connect == false){
            echo $this->twig->render('connection/logIn.html.twig', ["connectError" => true]);
        }
    }
}
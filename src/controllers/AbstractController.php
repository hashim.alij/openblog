<?php

namespace App\Controllers;
//  Symfony\Component\HttpFoundation\Session\Session;

abstract class AbstractController {
    protected $twig;

    public function __construct()
    {
        $loader = new \Twig\Loader\FilesystemLoader('./../templates/');
        $this->twig = new \Twig\Environment($loader);
        $this->twig->addGlobal('session', $_SESSION);
    }
}
<?php

namespace App\Controllers;

class BlogController extends AbstractController {
    public function index():void
    {
            $blogControl = new ArticleController();
            $articles = $blogControl->findAllValid();
            echo $this->twig->render('blog/blog.html.twig', ["articles" => $articles]);
    }

    public function article(string $id):void
    {
        $commentSent = "";
        $blogControl = new ArticleController();
        $article = $blogControl->find($id);

        $commentControl = new CommentController(); 
        if (isset($_POST["submit"])){
            $commentSent = $commentControl->create($id, $_POST['text']);
        }
        $state = "not connected";
        if (isset($_SESSION["idUser"])){
            $state = "connected";
        }

        $comments = $commentControl->articleComs($id);
        echo $this->twig->render('blog/article.html.twig', ["article" => $article, "comments" => $comments, "state" => $state, "commentSent" => $commentSent]);
    }
}
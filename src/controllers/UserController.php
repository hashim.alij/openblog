<?php 
namespace App\Controllers;
use App\Core\Database;
use App\Entities\User;

class UserController{
    private $db;
    
    public function __construct()
    {
        $this->db = Database::connect(); 
    }

    /**
     * @param User $user  
     */
    public function create(User $user){
        $lname = $user->getLname();
        $fname = $user->getFname();
        $role = $user->getRole();
        $username = $user->getUsername();
        $email = $user->getEmail();
        $password = password_hash($user->getPassword(), PASSWORD_DEFAULT);
 
        try {
            // set the \PDO error mode to exception
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO user (lname, fname, role, username, email, password)
            VALUES ('$lname', '$fname', '$role', '$username', '$email', '$password')";
            // use exec() because no results are returned
            $this->db->exec($sql);
          } catch(\PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
          }
    }

    /**
     * @param int $id  
     */
    public function find(int $id)
    {
        $stmt = "SELECT lname, fname, role, username, email from user where id = $id";
        $row = $this->db->query($stmt);
        $user = $row->fetch(\PDO::FETCH_OBJ);
        return $user;
    }

    /**
     * @param int $id  
     * @param string $lname  
     * @param string $fname  
     * @param string $username  
     * @param string $email  
     */
    public function update(int $id, string $lname, string $fname, string $username, string $email):bool{
        try {
            // set the \PDO error mode to exception
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE user SET lname='$lname', fname='$fname', username='$username', email='$email' WHERE id=".$id;
            // Prepare statement
            $stmt = $this->db->prepare($sql);
            // execute the query
            $stmt->execute();
            return true;
          } catch(\PDOException $e) {
            //  var_dump($sql . "<br>" . $e->getMessage());
            return false;
          }
    }

    /**
     * @param int $id  
     * @param string $password
     */
    public function updatePassword(int $id, string $password){
        try {
            $enc_password = password_hash($password, PASSWORD_DEFAULT);
            // set the \PDO error mode to exception
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE user SET password='$enc_password' WHERE id=".$id;
            // Prepare statement
            $stmt = $this->db->prepare($sql);
            // execute the query
            $stmt->execute();
            
            return " ";
          } catch(\PDOException $e) {
            return "passwordError";
          }
    }

    /**
     * @param int $id
     */
    public function delete(int $id){
        try{
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
              $sql = "DELETE FROM user WHERE id=".$id;
            
              $this->db->exec($sql);
              echo "Record deleted successfully";
            } catch(\PDOException $e) {
              echo $sql . "<br>" . $e->getMessage();
            }
    }

    /**
     * @param string $email
     * @param string $password
     */
    public function logIn(string $email, string $password):bool
    {
        try{
            $stmt = "SELECT * FROM user where email='$email'";
            $request = $this->db->query($stmt);
            $user = $request->fetch(\PDO::FETCH_OBJ);

            if(password_verify($password, $user->password)){
               $this->startSession($user);
               return true;
            } 
            else {
                return false;
            }
        } catch(\PDOException $e) {
            echo "Error: ". $e;
        }

    }

    /**
     * User $user
     */
    public function startSession($user):void{
        echo "session started!!";
        $_SESSION["idUser"]= $user->id;
        $_SESSION["fname"] = $user->fname;
        $_SESSION["username"] = $user->username;
        $_SESSION["role"] = $user->role;
        $_SESSION["state"] = "connected";
        header("Location: http://localhost:86/openblog/public/account");
    }
}
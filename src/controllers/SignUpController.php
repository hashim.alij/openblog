<?php

namespace App\Controllers;
use App\Entities\User;
use App\Controllers\UserController;

class SignUpController  extends AbstractController {
    public function index():void
    {
        echo $this->twig->render('connection/signUp.html.twig');
    }

    public function addUser():void
    {
        if ($_POST["password"] == $_POST["passwordR"])
        {
            $user = new User();
            $user->setLname(strtolower($_POST["lname"]));
            $user->setFname(strtolower($_POST["fname"]));
            $user->setRole('user');
            $user->setUsername(strtolower($_POST["username"]));
            $user->setEmail(strtolower($_POST["email"]));
            $user->setPassword($_POST["password"]);
            $userControl = new UserController();
            $userControl->create($user);
            $state = "signedUp";
        }
        else {
            $state = "passwordError";
        }

        echo $this->twig->render('connection/signUp.html.twig', ["state" => $state]);
    }
}
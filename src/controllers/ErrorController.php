<?php

namespace App\Controllers;

class ErrorController extends AbstractController {
    public function error404():void
    {
        echo $this->twig->render('error/error404.html.twig');
    }
}
<?php

namespace App\Controllers;

class AccountController  extends AbstractController {
    public function index():void
    {
        $blogControl = new ArticleController();
        $countArticles = $blogControl->count($_SESSION["idUser"]);
        $commentControl = new CommentController(); 
        $comCount = $commentControl->count($_SESSION["idUser"]);
        echo $this->twig->render('account/account.html.twig', ["articles" => $countArticles, "comments" => $comCount, ]);
    }

    public function profile($id):void
    {
        $userControl = new UserController();
        $user = $userControl->find($id);

        echo $this->twig->render('account/profile/profile.html.twig', ["user" => $user, "updateInfo" => ""]);
    }

    public function profilePassword($id):void
    {
        $updateInfo = "";

        $userControl = new UserController();
        if (isset($_POST["submitPassword"])){
            if ($_POST["password"] == $_POST["passwordR"]){
                $updateInfo = $userControl->updatePassword($_SESSION["idUser"], $_POST["password"]);
            } else {
                $updateInfo = "passwordError";
            }
        }
        $user = $userControl->find($id);
        echo $this->twig->render('account/profile/password.html.twig', ["user" => $user, "updateInfo" => $updateInfo, ]);
    }

    public function updateUser():void
    {
        $updateInfo = false;
        $userControl = new UserController();
        $updateInfo = $userControl->update($_SESSION["idUser"], strtolower($_POST["lname"]), strtolower($_POST["fname"]), strtolower($_POST["username"]), strtolower($_POST["email"]));

        $user = $userControl->find($_SESSION["idUser"]);
        $_SESSION["fname"] = $user->fname;
        $_SESSION["username"] = $user->username;
        echo $this->twig->render('account/profile/profile.html.twig', ["updateInfo" => $updateInfo, "user" => $user,]);
    }

    public function articles($id):void
    {
        $blogControl = new ArticleController();
        $articles = $blogControl->myArticles($id);
        echo $this->twig->render('account/articles/liste.html.twig', ["articles" => $articles]);
    }

    public function addArticle():void
    {
        $updateInfo = "";
        if(isset($_POST["submit"])){
            $articleControl = new ArticleController();
            $articleControl->create($_SESSION["idUser"], $_POST["author"],$_POST["lede"], $_POST["text"], $_POST["image"]);
            $updateInfo = "The article has been added.";
        } 
        echo $this->twig->render('account/articles/add.html.twig', ["updateInfo" => $updateInfo]);
    }

    public function updateArticle($id):void
    {
        $updateInfo = "";
        $article = new ArticleController();
        if (isset($_POST["submit"])){
            $article->update($id, $_SESSION["idUser"], $_POST["lede"], $_POST["author"], $_POST["text"], $_POST["image"]);
            $updateInfo = "L'article a été mis à jour";
        }
        $article = $article->find($id);
        echo $this->twig->render('account/articles/update.html.twig', ["article" => $article,"updateInfo" => $updateInfo]);
    }

    public function deleteArticle($id):void
    {
        $blogControl = new ArticleController();
        $blogControl->delete($id);
        $articles = $blogControl->myArticles($id);
        echo $this->twig->render('account/articles/liste.html.twig', ["articles" => $articles]);
        // header("Location: http://localhost:86/openblog/public/account/articles");
    }

    public function validateArticle($id):void
    {
        $blogControl = new ArticleController();
        $blogControl->validate($id);
        $article = $blogControl->find($id);
        $updateInfo = "The article has been updated";
        echo $this->twig->render('account/articles/update.html.twig', ["article" => $article,"updateInfo" => $updateInfo]);
    }
    
    public function unValidateArticle($id):void
    {
        $blogControl = new ArticleController();
        $blogControl->unValidate($id);
        $article = $blogControl->find($id);
        $updateInfo = "The article has been updated";
        echo $this->twig->render('account/articles/update.html.twig', ["article" => $article,"updateInfo" => $updateInfo]);
    }
    
    public function comments($id):void
    {
        $commentControl = new CommentController(); 
        $comments = $commentControl->myComments($id);
        echo $this->twig->render('account/comments/liste.html.twig', ["comments" => $comments]);
    }

    public function updateComment($id):void
    {
        
        $updateInfo = "";
        $commentControl = new CommentController();
        if (isset($_POST["submit"])){
            $commentControl->update($id, $_POST["text"]);
            $updateInfo = "The comment has been updated";
        }
        $comment = $commentControl->find($id);
        echo $this->twig->render('account/comments/update.html.twig', ["comment" => $comment,"updateInfo" => $updateInfo]);
    }

    public function deleteComment($id):void
    {
        $commentControl = new CommentController();
        $commentControl->delete($id);
        header("Location: http://localhost:86/openblog/public/account/comments");
    }

    public function validateComment($id):void
    {
        $commentControl = new CommentController();
        $commentControl->validate($id);
        $comment = $commentControl->find($id);
        $updateInfo = "The comment has been updated";
        echo $this->twig->render('account/comments/update.html.twig', ["comment" => $comment,"updateInfo" => $updateInfo]);
    }

    public function unValidateComment($id):void
    {
        $commentControl = new CommentController();
        $commentControl->unValidate($id);
        $comment = $commentControl->find($id);
        $updateInfo = "The comment has been updated";
        echo $this->twig->render('account/comments/update.html.twig', ["comment" => $comment,"updateInfo" => $updateInfo]);
    }

    public function adminArticles():void
    {
        $blogControl = new ArticleController();
        $articles = $blogControl->findAll();
        echo $this->twig->render('account/articles/liste.html.twig', ["articles" => $articles]);
    }
    
    public function adminComment():void
    {
        $commentControl = new CommentController(); 
        $comments = $commentControl->findAll();
        echo $this->twig->render('account/comments/liste.html.twig', ["comments" => $comments]);
    }
}
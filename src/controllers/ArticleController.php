<?php 
namespace App\Controllers;
use App\Core\Database;
use App\Entities\Commentaire;
use PDO;
use PDOException;

class ArticleController{
    private $db;
    
    public function __construct()
    {
        $this->db = Database::connect(); 
    }

    public function findAll()
    {
        $stmt = "SELECT * from article";
        $rows = $this->db->query($stmt);
        $articles = $rows->fetchAll();
        return $articles;
    }

    public function findAllValid()
    {
        $stmt = "SELECT * from article where status='valid'";
        $rows = $this->db->query($stmt);
        $articles = $rows->fetchAll();
        return $articles;
    }

    public function find($id)
    {
        $stmt = "SELECT * from article where id = $id";
        $row = $this->db->query($stmt);
        $article = $row->fetch(PDO::FETCH_OBJ);
        return $article;
    }

    public function create($idUser, $author, $lede, $text, $image){
        try {
            date_default_timezone_set('Europe/Paris');
            $date = date('Y-m-d'); 
            // set the PDO error mode to exception
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO article (id_user, status, lede, author, date, text, image)
            VALUES ('$idUser', 'pending', '$lede', '$author','$date', '$text', '$image')";
            $this->db->exec($sql);
          } catch(PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
          }
    }


    /**
     * @param int $id  
     * @param int $idUser  
     * @param string $lede  
     * @param string $author  
     * @param string $text  
     * @param string $image  
     */
    public function update(int $id, int $idUser, string $lede, string $author, string $text, string $image){
        try {
            date_default_timezone_set('Europe/Paris');
            $date = date('Y-m-d'); 
            // set the PDO error mode to exception
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE article SET id_user='$idUser', lede='$lede', author='$author', date='$date', text='$text', image='$image' WHERE id=".$id;
            // Prepare statement
            $stmt = $this->db->prepare($sql);
            // execute the query
            $stmt->execute();
          } catch(PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
          }
    }

    public function delete(int $id){
        try {
            // set the PDO error mode to exception
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "DELETE FROM article WHERE id='$id'";
            // Prepare statement
            $stmt = $this->db->prepare($sql);
            // execute the query
            $stmt->execute();
            // execute the query
          } catch(PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
          }
    }

    public function validate(int $id):bool{
      try {
          // set the PDO error mode to exception
          $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $sql = "UPDATE article SET status='valid' WHERE id='$id'";
          // Prepare statement
          $stmt = $this->db->prepare($sql);
          // execute the query
          $stmt->execute();
          return true;
        } catch(PDOException $e) {
          echo $sql . "<br>" . $e->getMessage();
        }
  }

    public function unValidate(int $id):bool{
      try {
          // set the PDO error mode to exception
          $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $sql = "UPDATE article SET status='pending' WHERE id='$id'";
          // Prepare statement
          $stmt = $this->db->prepare($sql);
          // execute the query
          $stmt->execute();
          return true;
        } catch(PDOException $e) {
          echo $sql . "<br>" . $e->getMessage();
        }
  }

    public function myArticles($id)
    {
        $stmt = "SELECT * from article where id_user = $id";
        $rows = $this->db->query($stmt);
        $articles = $rows->fetchAll();
        return $articles;
    }

    public function count($idUser){
      $stmt = "SELECT COUNT(*) FROM article where id_user=$idUser";
      $res = $this->db->query($stmt);
      $count = $res->fetchColumn();
      return $count;
    }
}